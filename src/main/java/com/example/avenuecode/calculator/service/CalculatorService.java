package com.example.avenuecode.calculator.service;

import com.example.avenuecode.calculator.controller.CalculatorController;
import com.example.avenuecode.calculator.exception.OperationTypeNotSupportedException;
import com.example.avenuecode.calculator.model.CalculatorResponse;
import com.example.avenuecode.calculator.model.OperationType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class CalculatorService {

    private final Logger logger
            = LoggerFactory.getLogger(this.getClass());

    public CalculatorResponse calculate(Double principalAmount, int years, Double rate, String type)
            throws OperationTypeNotSupportedException {

        logger.info(
                "Method 'calculate' called with params: " +
                        "amount= {}, " +
                        "years= {}, " +
                        "rate= {}, " +
                        "type= '{}\'",
                principalAmount, years, rate, type);

        Double interestAmount;
        if(type.equals(OperationType.SIMPLE_INTEREST.getOperationType())){
            //formula for simple interest
            interestAmount = principalAmount * years * rate/100;
        }
        else if(type.equals(OperationType.COMPOUND_INTEREST.getOperationType())){
            //formula for compound interest
            interestAmount = principalAmount * Math.pow((1 + (rate/100)), years) - principalAmount;
        }
        else {
            logger.error("Method 'calculate' threw {}. Type not supported: '{}'",
                    OperationTypeNotSupportedException.class.getSimpleName(), type);
            throw new OperationTypeNotSupportedException(type);
        }

        Double totalAmount = interestAmount + principalAmount;

        CalculatorResponse res = new CalculatorResponse(
                truncate(interestAmount),
                truncate(totalAmount),
                rate,
                type);

        logger.info("Method 'calculate' returned {}", res);

        return res;
    }

    private double truncate(double value) {
        logger.debug("Method truncate called with value {}", value);
        return Math.round(value * 100) / 100d;
    }
}
