package com.example.avenuecode.calculator.controller;

import com.example.avenuecode.calculator.model.CalculatorResponse;
import com.example.avenuecode.calculator.service.CalculatorService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CalculatorController {

    CalculatorService calculatorService;

    private final Logger logger =
            LoggerFactory.getLogger(this.getClass());

    @Autowired
    public CalculatorController(CalculatorService calculatorService) {
        this.calculatorService = calculatorService;
    }

    @GetMapping("/interest")
    public ResponseEntity<CalculatorResponse> calculateInterest(
            @RequestParam Double amount,
            @RequestParam int years,
            @RequestParam Double rate,
            @RequestParam String type) {

            logger.info(
                    "Method 'calculateInterest' called with params: " +
                            "amount= {}, " +
                            "years= {}, " +
                            "rate= {}, " +
                            "type= '{}\'",
                    amount, years, rate, type);

            ResponseEntity<CalculatorResponse> res =  ResponseEntity.status(HttpStatus.OK)
                    .body(calculatorService
                            .calculate(amount, years, rate, type));

            logger.info("Method 'calculateInterest' returned: {}",
                    res.getBody());

            return res;
    }
}
