package com.example.avenuecode.calculator.exception;

public class OperationTypeNotSupportedException extends RuntimeException {
    public OperationTypeNotSupportedException(String type) {
        super(String.format("Operation type \'%s\' not supported", type));
    }
}
