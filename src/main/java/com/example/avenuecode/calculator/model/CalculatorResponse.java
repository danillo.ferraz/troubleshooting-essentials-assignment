package com.example.avenuecode.calculator.model;

public class CalculatorResponse {

    private double interestAmount;
    private double totalAmount;
    private double rate;
    private String type;

    public CalculatorResponse(double interestAmount, double totalAmount, double rate, String type) {
        this.interestAmount = interestAmount;
        this.totalAmount = totalAmount;
        this.rate = rate;
        this.type = type;
    }

    public double getInterestAmount() {
        return interestAmount;
    }

    public void setInterestAmount(double interestAmount) {
        this.interestAmount = interestAmount;
    }

    public double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "CalculatorResponse{" +
                "interestAmount=" + interestAmount +
                ", totalAmount=" + totalAmount +
                ", rate=" + rate +
                ", type='" + type + '\'' +
                '}';
    }
}
