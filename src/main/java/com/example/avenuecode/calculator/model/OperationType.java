package com.example.avenuecode.calculator.model;

public enum OperationType {
    SIMPLE_INTEREST("simple"),
    COMPOUND_INTEREST("compound");

    private String operationType;

    OperationType(String operationType) {
        this.operationType = operationType;
    }

    public String getOperationType() {
        return this.operationType;
    }
}
