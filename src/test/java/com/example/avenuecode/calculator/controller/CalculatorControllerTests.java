package com.example.avenuecode.calculator.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest
public class CalculatorControllerTests {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void simpleInterestCalculator_shouldReturnTotalInterest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .get("/interest?amount=100&years=1&rate=3&type=simple"))
                .andExpect(status().isOk())
                .andExpect(
                        jsonPath("$.total")
                        .value(103.0)
                );

    }
}
