# ACP2P Engineering Essentials
## Week 3 - Troubleshooting Essentials Assignment
### Interest Calculator App

This is an application that calculates the interest given some parameters e.g.:

```http://localhost:8080/interest?amount=1000&years=30&rate=15&type=compound```

The response will be something like:

```{"interestAmount":65211.77,"totalAmount":66211.77,"rate":15.0,"type":"compound"}```

Supported types: ```'simple'``` and ```'compound'```

#### The main focus of this week's assignment is to provide a troubleshooting approach for an application lifecycle

The application log is written with ```logback```, fetched by ```Logstash``` locally </br> and sent to ```Elasticsearch```, then displayed on ```Kibana```through the elastic cluster, aka ```ELK Stack```.

Also there is a Java Agent that sent statistics to the ```APM Server``` in the elastic platform.

Last but not least, ```VisualVM``` profiler was used to monitor the application.
